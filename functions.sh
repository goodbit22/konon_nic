#!/bin/bash

RED=$(tput setaf 1)                                                                                           
GREEN=$(tput setaf 2)                                                                                         
WHITE=$(tput setaf 7)                                                                                         
CYAN=$(tput setaf 6)

print_std () {
    printf "\t\033[34;1m[*]%s\033[0m\n" "$1"
}

print_phase () {
    printf "\n\033[01;33m[PHASE:]%s\033[0m\n" "$1"
}

print_succ () {
    printf "\n\t\033[32;1m[+]%s\033[0m\n" "$1"
}

print_failure () {
    printf "\n\t\033[01;31m[-]%s\033[0m\n" "$1"
}

failure () {
    print_failure "$1"
    exit 1
}

view_banner(){
    echo "${CYAN}
_|    _|                                          
 _|  _|      _|_|    _|_|_|      _|_|    _|_|_|    
_|_|      _|    _|  _|    _|  _|    _|  _|    _|  
_|  _|    _|    _|  _|    _|  _|    _|  _|    _|  
_|    _|    _|_|    _|    _|    _|_|    _|    _

    ${WHITE}"
}

menu_main(){
    if [ "$1" =  "dict" ]; then
echo "1) Attack by hydra
2) Attack by ncrack
3) Usage of rcpclient
4) Usage of snmpwalk & enum4linux
5) Usage of hashid
6) Usage of john
7) Usage of unshadow
8) Usage of hashcat
9) Exit
    "
else
echo "1) Usage of rcpclient
2) Usage of snmpwalk & enum4linux
3) Usage of hashid
4) Usage of john
5) Usage of unshadow
6) Usage of hashcat
7) Exit
    "
fi
}

usage(){
    echo "Usage: ${0} [OPTION]... [FILE]..."
    echo "Examples:
        ./${0} -l file.txt -p  file.txt    # Dictionary attack mode using different dictionaries for passwords and loginss.
        ./${0} -l file.txt                 # Dictionary attack mode using the same dictionary for passwords and logins
        ./${0}                             # Brute force attack mode.
    "
}

create_directory(){
    dir='result_konon'
    [[ ! -d "$dir" ]] && mkdir "$dir"
}

port_available(){
    host="$1"
    port="$2"
    state=$(nmap -p "$port" "$host" | grep "$port" | grep open)
    if [ -z "$state" ]; then
        echo "${RED}Connection to $host on port $port has failed${WHITE}"
        return 1
    else
        echo "${GREEN}Connection to $host on port $port was successful${WHITE}"
        return 0
    fi
}

test_address(){
    echo "$1" | grep -E '(([0-9]{1,3})\.){3}([0-9]{1,3}){1}'  | grep -vE '25[6-9]|2[6-9][0-9]|[3-9][0-9][0-9]' &> '/dev/null' 
}

