#!/bin/bash

hashcat_linux_password_brute(){
    read -rp "Enter hash:  " hash_hashcat
    print_succ 'starting Brute force attack';
    hashcat -m 1800 -a 3  "$hash_hashcat" '?l?l?l?l?l?l?l' -o crackedpassword_linbrute.txt
    [ "$?" -eq 1 ] && echo "${RED}An incorrect hash was entered${WHITE}"
}

hashcat_windows_password_brute(){
    read -rp "Enter hash: " hash_hashcat
    print_succ 'starting Brute force attack';
    hashcat -m 1000 -a 3  "$hash_hashcat" '?l?l?l?l?l?l?l' -o crackedpassword_winbrute.txt
    [ "$?" -eq 1 ] && echo "${RED}An incorrect hash was entered${WHITE}"
}

hashcat_usage_brute(){
    local opts=("Unix password" "Windows password" "Back")
    PS3="Select options: "
    select option in "${opts[@]}"                                                                            
    do                                                                                                          
    case $option in                                                                                           
        "${opts[0]}") 
            echo "${CYAN}you have selected the option: $REPLY ${WHITE}"
            hashcat_linux_password_brute
            break                                                                                                                                                                                                                                                                  
            ;;                                                                                        
        "${opts[1]}")                                                                                  
            echo "${CYAN}you have selected the option:  $REPLY ${WHITE}" 
            hashcat_windows_password_brute                                                                                          
            break
           ;;
        "${opts[2]}")                                                                                  
            echo "${CYAN}you have selected the option: $REPLY ${WHITE}"                                                                                                           
            break
            ;;
        *) echo -e "${RED}Invalid option: $REPLY ${WHITE}"        
    esac                                                                                                      
    done                         
}