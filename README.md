# Konon

The Bash script for Kali Linux distribution allows dictionary and brute force attacks on ftp, ssh, rcp protocols among others

![graphic1](./img/1.jpg)

## Table of Content

* [Features](#features)
* [Technologies](#technologies)
* [Running](#running)
* [Author](#author)

## Prerequisites

install bats-core 'https://github.com/bats-core/bats-core'  to run tests

## Features

1. hash identification
2. Linux password attack
3. Windows password attack
4. Attacking ftp, ssh, rcp protocols automated by hydra and ncrack

## Technologies

* bash 5.0
* awk 5.0.1
* bats

## Running

```sh
 sudo bash  main.bash
```

or

```sh
 sudo ./konon.bash
```

## Running tests

```bats
    bats ./test/functions.bats
```

## Author

*****
__goodbit22__ --> <https://gitlab.com/users/goodbit22>
*****
